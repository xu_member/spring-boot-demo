package com.xkcoding.log.aop;

import cn.hutool.core.lang.Dict;
import com.xkcoding.log.aop.controller.TestController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootDemoLogAopApplicationTests {

    @Autowired
    TestController testController;
    @Test
    public void contextLoads() {
        Dict test = testController.test("'胥员员");
        System.out.println(test);
    }

}
