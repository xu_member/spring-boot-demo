package com.xkcoding.mq.rabbitmq;

import com.xkcoding.mq.rabbitmq.config.RabbiMqConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringBootDemoMqRabbitmqApplicationTests {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 测试直接模式发送
     */
    @Test
    public void sendDirect() {
            rabbitTemplate.convertAndSend(RabbiMqConfig.EXCHANGE_RISK_API_TASK, RabbiMqConfig.KEY_RISK_API_TASK, "sss");
    }

}

