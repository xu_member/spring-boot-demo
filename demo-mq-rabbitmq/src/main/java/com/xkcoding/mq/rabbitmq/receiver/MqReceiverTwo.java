package com.xkcoding.mq.rabbitmq.receiver;


import com.xkcoding.mq.rabbitmq.config.BaseDtoMessageConverter;
import com.xkcoding.mq.rabbitmq.config.RabbiMqConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

/**
 * [Description]: 消费
 *
 * @author : xh
 * @date : 2021-08-13 11:04
 */
@Slf4j
//@Component
//@AllArgsConstructor
public class MqReceiverTwo {

    BaseDtoMessageConverter converter;
    /**
     * 消费 报告生成2
     * RabbiMqConfig
     *
     * @param messages 消费数据
     */
    @RabbitHandler
    @RabbitListener(queues = {RabbiMqConfig.QUEUE_RISK_API_TASK})
    public void interior(Message messages) {
        try {
            String json = converter.fromMessage(messages);
            log.info("消费者2:{}", json);
//            Thread.sleep(5000L);
        } catch (Exception e) {
            log.error("=============报告生成-消费队列报错，错误原因", e);
        }
    }

}
