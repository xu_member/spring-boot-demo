package com.xkcoding.mq.rabbitmq.receiver;


import lombok.Data;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.Map;

/**
 * [Description]: 多线程执行返回值
 *
 * @author : xh
 * @date : 2021-08-12 16:58
 */
@Data
@Accessors(chain = true)
public class ApiTaskDto {
    private String id;
    private int number = 0;
    private Boolean isOk = true;
    private Boolean analyseValue;
    private String returnedValue;
}
